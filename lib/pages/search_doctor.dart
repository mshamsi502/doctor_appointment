import 'dart:ui';
import 'package:doctor_appointment/pages/pick_date.dart';
import 'package:doctor_appointment/pages/specialist_doctors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class SearchDoctor extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SearchDoctorState();
}

class SearchDoctorState extends State<SearchDoctor>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<String> _dropDownItems = <String>['Tehran', 'Mississippi','Kalyan Nagar',];
  String _dropDownValue;
  String _selectDate;
  String _keySearch;
  String _choosedTime;
  TextEditingController _textEditingController = new TextEditingController();
  // sat: 0
  // mon: 1
  // tue: 2
  // wed: 3
  // thu: 4
  // fri: 5
  // sun: 6
  @override
  void initState() {
    _tabController = new TabController(initialIndex: 0, length: 3, vsync: this);
    _dropDownValue = _dropDownItems[2];
    _keySearch = '';
    _choosedTime = 'Your Selected Time ${DateTime.now().weekday}th Day of Week,'+' All Time of Day';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;


    Container _backButton = new Container(
        margin: const EdgeInsets.only(left: 15, top: 10),
        alignment: Alignment.topLeft,
        child: new GestureDetector(
          child: new Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ));
    Positioned _homeAppBar = new Positioned(
        child: new Stack(alignment: Alignment.topCenter, children: [
      new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          new Container(
              alignment: Alignment.topCenter,
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                fit: BoxFit.fill,
                image: new AssetImage('assets/images/cov.png'),
              )),
              padding: const EdgeInsets.only(left: 20, right: 20),
              height: _screenSize.height * 0.25,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  new SizedBox(
                    height: 62,
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          new Text(
                            'Select',
                            style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.w300,
                                color: Colors.white,
                                letterSpacing: 0.8),
                          ),
                          new Text(
                            ' Doctor',
                            style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                                letterSpacing: 0.8),
                          ),
                        ],
                      ),
                      new Container(
                          child: new DropdownButton(
                              value: _dropDownValue,
                              dropdownColor: Color(0xff5189f9),
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: Colors.white,
                              ),
                              iconSize: 20,
                              items: _dropDownItems
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value,
                                      style: TextStyle(color: Colors.white)),
                                );
                              }).toList(),
                              onChanged: (String newValue) {
                                setState(() {
                                  _dropDownValue = newValue;
                                });
                              }))
                    ],
                  ),
                ],
              )),
        ],
      ),
    ]));
    Positioned _searchBar = new Positioned(
      left: _screenSize.width * 0.1,
      child: new Container(
        width: _screenSize.width * 0.8,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: new BorderRadius.all(const Radius.circular(40))),
        child: new Container(
          margin: const EdgeInsets.only(right: 5, left: 5, bottom: 5),
          decoration: BoxDecoration(boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey[600],
                blurRadius: 1,
                offset: Offset.fromDirection(-10)),
          ], borderRadius: BorderRadius.circular(100), color: Colors.white),
          child: new Row(
            children: [
              new Expanded(
                child: new Container(
                    padding: const EdgeInsets.only(left: 15),
                    child: new TextField(
                      controller: _textEditingController,
                      decoration: new InputDecoration(
                          hintText: "Search ...", border: InputBorder.none),
                    )),
              ),
              new IconButton(
                  icon: new Icon(
                    Icons.search,
                    color: Colors.black,
                    size: 30,
                  ),
                  onPressed: () {
                    print(_textEditingController.value.text);
                    _keySearch = _textEditingController.value.text;
                  }),
            ],
          ),
        ),
      ),
    );

    String changeSelectedTime(String date) {
      String _selectedTime = '';
      if(date.substring(2,date.length-2) == 'morning') {
        switch(date.substring(date.length-1)) {
          case '0':
            {
              _selectedTime = date.substring(0,1) + ',----';
              break;
            }
          case '1':
            {
              _selectedTime = date.substring(0,1) + ',0830';
              break;
            }
          case '2':
            {
              _selectedTime = date.substring(0,1) + ',0900';
              break;
            }
          case '3':
            {
              _selectedTime = date.substring(0,1) + ',0930';
              break;
            }
          case '4':
            {
              _selectedTime = date.substring(0,1) + ',1000';
              break;
            }
          case '5':
            {
              _selectedTime = date.substring(0,1) + ',1030';
              break;
            }
          case '6':
            {
              _selectedTime = date.substring(0,1) + ',1130';
              break;
            }
        }
      }
      if(date.substring(2,date.length-2) == 'afternoon') {
        switch(date.substring(date.length-1)) {
          case '0':
            {
              _selectedTime = date.substring(0,1) + ',----';
              break;
            }
          case '1':
            {
              _selectedTime = date.substring(0,1) + ',1200';
              break;
            }
          case '2':
            {
              _selectedTime = date.substring(0,1) + ',1230';
              break;
            }
          case '3':
            {
              _selectedTime = date.substring(0,1) + ',1330';
              break;
            }
          case '4':
            {
              _selectedTime = date.substring(0,1) + ',1400';
              break;
            }
          case '5':
            {
              _selectedTime = date.substring(0,1) + ',1430';
              break;
            }
          case '6':
            {
              _selectedTime = date.substring(0,1) + ',1530';
              break;
            }
        }
      }
      if(date.substring(2,date.length-2) == 'evening') {
        switch(date.substring(date.length-1)) {
          case '0':
            {
              _selectedTime = date.substring(0,1) + ',----';
              break;
            }
          case '1':
            {
              _selectedTime = date.substring(0,1) + ',1600';
              break;
            }
          case '2':
            {
              _selectedTime = date.substring(0,1) + ',1630';
              break;
            }
          case '3':
            {
              _selectedTime = date.substring(0,1) + ',1730';
              break;
            }
          case '4':
            {
              _selectedTime = date.substring(0,1) + ',1800';
              break;
            }
          case '5':
            {
              _selectedTime = date.substring(0,1) + ',1830';
              break;
            }
          case '6':
            {
              _selectedTime = date.substring(0,1) + ',1930';
              break;
            }
        }
      }
      print(_selectedTime);
      return _selectedTime;
    }

    Widget resultSearch(String date) {
      if(date != null) {
        setState(() {
          _choosedTime =
              date.substring(date.length-1) == '0'
              ? 'Your Selected Time ${date.substring(0, 1)}th Day of Week, All Time of Day'
              : 'Your Selected Time ${date.substring(0, 1)}th Day of Week, ${changeSelectedTime(date).substring(2,4)}:${changeSelectedTime(date).substring(4,6)}'
              ;
        });
      }
        return new Container(
          padding: const EdgeInsets.symmetric(horizontal: 20,),
          child: new DraggableScrollableSheet(
              initialChildSize: 0.89,
              minChildSize: 0.89,
              maxChildSize: 0.9,
              builder: (context, _resScrollController) => Container(
                  child: new SingleChildScrollView(
                    controller: _resScrollController,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        new Text(_choosedTime),
                        new SizedBox(height: 10,),
                        new Container(
                          height: _screenSize.height * 0.75,
                          child: new SpecialistDoctors(special :'search', keySearch: _keySearch, date: changeSelectedTime(date),),
//                              new SizedBox(height: 20,)
                         ),
                        new SizedBox(height: 10,),
                      ],
                    ),
                  )
              )
          )
      );
    }

      Container _results = new Container (
        height: _screenSize.height,
        child: new TabBarView(
          controller: _tabController,
          children: <Widget>[
            new Container(child: resultSearch(DateTime.now().weekday.toString()+',morning,0')),
            new Container(child: resultSearch((DateTime.now().weekday + 1).toString()+',morning,0')),
            new Container(child: resultSearch(_selectDate)),
          ],
        ),
    );

    Container _timeTabBar = new Container(
          alignment: Alignment.topCenter,
          padding: const EdgeInsets.only(top: 35),
          margin: const EdgeInsets.only(bottom: 10),
          height: 70,
          child: new TabBar(
            labelPadding: EdgeInsets.symmetric(horizontal: 10),
            isScrollable: true,
            indicator: BoxDecoration(
              color: Color(0xff0148fa),
              borderRadius: new BorderRadius.all(const Radius.circular(30)),
            ),
            indicatorSize: TabBarIndicatorSize.label,
            controller: _tabController,
            unselectedLabelColor: Colors.grey,
            unselectedLabelStyle: TextStyle(
                fontSize: 14, fontWeight: FontWeight.w500, color: Colors.grey),
            labelStyle: TextStyle(
                fontWeight: FontWeight.w800, fontSize: 14, color: Colors.white),
            labelColor: Colors.white,
            tabs: <Widget>[
              new Tab(child: new GestureDetector(
                child: new Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey),
                      borderRadius: new BorderRadius.all(const Radius.circular(30)),
                    ),
                    child: new Text(
                      "Today",
                    )),
                onTap: () {
                    _choosedTime = 'Your Selected Time ${DateTime.now().weekday}th Day of Week, FullTime';
                    _tabController.index = 0;
                },
              ),
              ),
              new Tab(child: new GestureDetector(
                child: new Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey),
                      borderRadius: new BorderRadius.all(const Radius.circular(30)),
                    ),
                    child: new Text(
                      "Tomorrow",
                    ),
                ),
                onTap: () {
                    _choosedTime = 'Your Selected Time ${(DateTime.now().weekday) + 1}th Day of Week, FullTime';
                    _tabController.index = 1;

                },
              ),
              ),
              new Tab(
                child: new GestureDetector(
                  child:  new Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                        borderRadius: new BorderRadius.all(const Radius.circular(30)),
                      ),
                      child: new Row(
                        children: [
                          new Text('Pick a Date'),
                          new SizedBox(
                            width: 5,
                          ),
                          new Icon(
                            Icons.date_range,
                            size: 20,
                          )
                        ],
                      )
                  ),
                  onTap: () async {
                    _selectDate = await Navigator.push(context, new MaterialPageRoute(builder: (context) => PickDate()));
                      if(_selectDate == null) {
                        _tabController.index = 0;
                      }
                      else {
                        _tabController.index = 2;
                      }

                  },
                ),
              ),
            ],
          ),
        );

    Container _information = new Container(
      child: new DraggableScrollableSheet(
          initialChildSize: 0.78,
          minChildSize: 0.78,
          maxChildSize: 0.92,
          builder: (context, _infoScrollController) => Container(
                  child: new Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                ),
                child: new Stack(
                  children: [
                    new DraggableScrollableSheet(
                    initialChildSize: 0.91,
                    minChildSize: 0.91,
                    maxChildSize: 0.95,
                    builder: (context, infoScrollController) => Container(
                      child: new Container(
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                        ),
                        child: new Stack(
                          children: [
                            new Positioned(
                              child: new Container(
//                                width: _screenSize.width * 0.4,
                                decoration: BoxDecoration(
                                  color: Colors.white,),
                                child: new SingleChildScrollView(
                                  controller: infoScrollController,
                                  child: _results,
                                ),
                              ),
                            ),
                          ],
                        ),

                      )
                    )
                    ),
                    new Positioned(
                      top: _screenSize.height * 0.04,
                      child: new Container(
                        width: _screenSize.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(50)),
                        child: new SingleChildScrollView(
                            controller: _infoScrollController,
                              child: _timeTabBar,
                            ),
                      ),
                    ),
                    _searchBar,
                  ],
                ),
              ))),
    );

    return new Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: 0,
      ),
      body: new Container(
        height: _screenSize.height,
        alignment: Alignment.topCenter,
        child: new Stack(
          alignment: Alignment.topCenter,
          children: [_homeAppBar, _backButton, _information],
        ),
      ),
    );
  }
}
