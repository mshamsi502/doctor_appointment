import 'package:doctor_appointment/models/doctor.dart';
import 'package:doctor_appointment/pages/doctorScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';


 class ResultSearch {
  final String date;
  final int startTime;
  final int endTime;
  final String keySearch;
  List<Doctor> _resultList = <Doctor> [];




  ResultSearch({
    @required this.date,
    @required this.startTime,
    @required this.endTime,
    @required this.keySearch
  });

  Widget _myListViewBuilder(List<Doctor> _doctorsList) {
    return ListView.builder(
        itemCount: _doctorsList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return new Row(children: [
            new SizedBox(
              width: 15,
            ),
            new GestureDetector(
              onTap: () {
                Navigator.push(context,  new MaterialPageRoute(
                    builder: (context) => new DoctorScreen(doctor: _doctorsList[index])
                ));
              },
              child: Container(
                  margin: const EdgeInsets.only(bottom: 10, top: 10),
                  decoration: BoxDecoration(
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey[600],
                            blurRadius: 4,
                            offset: Offset.fromDirection(-10)),
                      ],
                      color: Color(0xfff3f6ff),
                      borderRadius: BorderRadius.circular(15)),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      new Container(
                        alignment: Alignment.topCenter,
                        height: 140,
                        width: 140,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                              image: _doctorsList[index].image.image,
                            )),
                      ),
                      new SizedBox(
                        height: 5,
                      ),
                      new Container(
                        margin: const EdgeInsets.only(
                            left: 10, right: 10, bottom: 10),
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text(
                                'Dr. ${_doctorsList[index].firstName} ${_doctorsList[index].lastName}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              new Text(
                                _doctorsList[index].special,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.grey),
                              ),
                              new SizedBox(
                                height: 3,
                              ),
                              new Row(
                                children: [
                                  new SmoothStarRating(
                                      allowHalfRating: false,
                                      onRated: (v) {},
                                      starCount: 5,
                                      rating: _doctorsList[index].rate,
                                      size: 15.0,
                                      isReadOnly: true,
                                      filledIconData: Icons.star,
                                      halfFilledIconData: Icons.star_half,
                                      color: Colors.amber,
                                      borderColor: Colors.amber,
                                      spacing: 0.0),
                                  new SizedBox(
                                    width: 5,
                                  ),
                                  new Text(
                                    _doctorsList[index].rate.toString(),
                                    style: TextStyle(color: Colors.amber),
                                  ),
                                  new SizedBox(
                                    width: 10,
                                  ),
                                  new Icon(
                                    Icons.arrow_forward,
                                    color: Colors.grey,
                                    size: 15,
                                  ),
                                ],
                              ),
                            ]),
                      )
                    ],
                  )),
            ),
          ]);
        });
  }

  Widget build(BuildContext context) {
    return _myListViewBuilder(_resultList);
  }

   searchDoctor(List<Doctor> _doctorList, int _navigationDoctorNum) {
    if(keySearch != '') {
      //
    } else {
      for(var i = 0 ; i < _doctorList.length ; i++ ) {
        _doctorList[i].workingDay[this.date]
        ? _doctorList[i].workingTimeStart >= this.startTime && _doctorList[i].workingTimeEnd < this.endTime
            ? _resultList.add(_doctorList[i])
            // ignore: unnecessary_statements
            : null
        // ignore: unnecessary_statements
        : null
        ;
      }
    }
  }

}
