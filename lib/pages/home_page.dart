import 'dart:ui';
import 'package:doctor_appointment/models/doctor.dart';
import 'package:doctor_appointment/pages/search_doctor.dart';
import 'package:doctor_appointment/pages/specialist_doctors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<String> _dropDownItems = <String> [
    'Tehran', 'Mississippi','Kalyan Nagar'
  ];
  String _dropDownValue;

  @override
  void initState() {
    _tabController = new TabController(initialIndex: 0, length: 4, vsync: this);
    _dropDownValue = _dropDownItems[2];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery
        .of(context)
        .size;

    Widget _dailyAppointments(Doctor doctor, String date, String time) {
      return new Container(
          margin: const EdgeInsets.only(bottom: 2),
          padding:
          const EdgeInsets.only(top: 15, bottom: 15, left: 7, right: 7),
          width: 235,
          height: 90,
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(color: Colors.grey[800], blurRadius: 3.0),
            ],
            borderRadius: BorderRadius.circular(12),
            color: Color(0xfff3f6ff),
          ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              doctor.avatar != null
                  ? new CircleAvatar(
                child: doctor.avatar,
                backgroundColor: Color(0xfff0f0f0),
              )
                  : new CircleAvatar(
                backgroundColor: Colors.grey,
              ),
              new SizedBox(
                width: 5,
              ),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  new Text(
                    'Dr. ${doctor.firstName} ${doctor.lastName}',
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),
                  ),
                  new Text(doctor.special,
                      style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey)),
                  new SizedBox(
                    height: 5,
                  ),
                  new Text(date,
                      style:
                      TextStyle(fontSize: 13, fontWeight: FontWeight.w600)),
                ],
              ),
                  new Container(
                    padding: const EdgeInsets.only(
                      left: 10,
                    ),
                    alignment: Alignment.bottomRight,
                    child: new Text(time,
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Colors.black45)),
              )

            ],
          )
      );
    }

    AppBar _homeAppBar = new AppBar(
        elevation: 0,
        shadowColor: Colors.transparent,
        backgroundColor: Color(0xff0148fa),
        bottom: new PreferredSize(
            preferredSize: Size.fromHeight(_screenSize.height * 0.25),
            child: new Stack(alignment: Alignment.bottomCenter, children: [
              new Column(
                children: [
                  new Container(
                      decoration: new BoxDecoration(
                          image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: new AssetImage('assets/images/cov.png'),
                          )),
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      height: _screenSize.height * 0.25,
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Column(
                                children: [
                                  new Text(
                                    'Hello Nevil!',
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white),
                                  ),
                                  new Text(
                                    "Hope you are well today",
                                    style: TextStyle(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                ],
                              ),
                              new Container(
                                child: new DropdownButton(
                                  value: _dropDownValue,
                                    dropdownColor: Color(0xff5189f9),
                                    icon: Icon(Icons.keyboard_arrow_down, color: Colors.white,),
                                    iconSize: 20,
                                    items: _dropDownItems.map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                    value: value,
                                child: Text(value, style: TextStyle(color: Colors.white)),
                                    );
                                }).toList(),
                                    onChanged: (String newValue) {
                                    setState(() {
                                      _dropDownValue = newValue;
                                    });
                                    }
                                )
                              )
                            ],
                          ),
                          new SizedBox(
                            height: 25,
                          ),
                          new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Text('DONT FORGET',
                                    style: TextStyle(color: Colors.white38)),
                                new Text('Clear',
                                    style: TextStyle(color: Colors.black)),
                              ]),
                        ],
                      )),
                  new Container(
                    color: Colors.white,
                    height: 50,
                  )
                ],
              ),
              new Container(
                padding: const EdgeInsets.only(
                  bottom: 10,
                ),
                height: 100,
                child: new ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                  new SizedBox(
                  width: 20,
                ),
                _dailyAppointments(
                    SpecialistDoctors(special: 'psycology')
                        .getPsycologyDoctor()[2],
                    "Wed, 02 Sep'20",
                    '03:00 pm'
                ),
                new SizedBox(
                  width: 15,
                ),
                _dailyAppointments(
                    SpecialistDoctors(special: 'cardiology')
                        .getCardiologyDoctor()[1],
                    "Sun, 12 Sep'20",
                    '09:00 am'
                ),
                    new SizedBox(
                      width: 20,
                    ),
    ]
              )
    )
            ]
            )
        )
    );
    Container _specialisedIn = new Container(
      child: new Column(
        children: [
          new Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            new SizedBox(width: 20,),
            new SizedBox(
              width: 150,
              child: new Text(
                'Specialised in',
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Colors.black),
              ),
            ),

            new SizedBox(width: _screenSize.width * 0.25),
            new Row(
              children: [
                new Text(
                  'View All',
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                      color: Colors.blueAccent),
                ),
                new SizedBox(width: 5,),
                new Icon(Icons.play_arrow, color: Colors.blueAccent,),
              ],
            ),
            new SizedBox(
              width: 20,
            )
          ]),
          new Container(
              padding: const EdgeInsets.only(bottom: 10, top: 10),
              height: 90,
              child: new ListView(scrollDirection: Axis.horizontal, children: [
                new SizedBox(
                  width: 20,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Color(0xffecf0fc),
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                  const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Image(
                        image: AssetImage('assets/images/bodyIcons/liver.png'),
                        height: 40,
                        width: 40,
                      ),
                      new Text(
                        'Liver',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                new SizedBox(
                  width: 10,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Color(0xffecf0fc),
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                  const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Image(
                        image: AssetImage('assets/images/bodyIcons/lungs.png'),
                        height: 40,
                        width: 40,
                      ),
                      new Text(
                        'Lungs',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                new SizedBox(
                  width: 10,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Color(0xffecf0fc),
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                  const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Image(
                        image: AssetImage('assets/images/bodyIcons/heart.png'),
                        height: 40,
                        width: 40,
                      ),
                      new Text(
                        'Heart',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                new SizedBox(
                  width: 10,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Color(0xffecf0fc),
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                  const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Image(
                        image: AssetImage('assets/images/bodyIcons/kidney.png'),
                        height: 40,
                        width: 40,
                      ),
                      new Text(
                        'Kidney',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                new SizedBox(
                  width: 10,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Color(0xffecf0fc),
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                  const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Image(
                        image: AssetImage('assets/images/bodyIcons/brain.png'),
                        height: 40,
                        width: 40,
                      ),
                      new Text(
                        'Brain',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                new SizedBox(
                  width: 10,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Color(0xffecf0fc),
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                  const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Image(
                        image: AssetImage('assets/images/bodyIcons/ear.png'),
                        height: 40,
                        width: 40,
                      ),
                      new Text(
                        'Ears',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                new SizedBox(
                  width: 10,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Color(0xffecf0fc),
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                  const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Image(
                        image: AssetImage('assets/images/bodyIcons/joint.png'),
                        height: 40,
                        width: 40,
                      ),
                      new Text(
                        'Joints',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
                new SizedBox(
                  width: 10,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Color(0xffecf0fc),
                      borderRadius: BorderRadius.circular(15)),
                  padding:
                  const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Image(
                        image: AssetImage('assets/images/bodyIcons/tooth.png'),
                        height: 40,
                        width: 40,
                      ),
                      new Text(
                        'Tooth',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey[600],
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                ),
              ])),
        ],
      ),
    );
    Container _doctors = new Container(
        child: new Column(children: [
          new Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              new SizedBox(width: 20,),
              new SizedBox(
                  width: 150,
                  child:
            new Text(
              'Doctors',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Colors.black),
            ),
              ),
            new SizedBox(width: _screenSize.width * 0.25),
            new GestureDetector(
              child: new Row(
                children: [
                  new Text(
                    'Search',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Colors.blueAccent),
                  ),
                  new SizedBox(width: 5,),
                  new Icon(Icons.search, color: Colors.blueAccent,),
                ],
              ),
              onTap: () {
                Navigator.push(context,  new MaterialPageRoute(
                    builder: (context) => new SearchDoctor()
                ));
              },
            ),
            new SizedBox(
              width: 20,
            )
          ]),
          new SizedBox(
            height: 10,
          ),
          new Column(
            children: [
              new Container(
                height: 40,
                child: new TabBar(
                  isScrollable: true,
                  controller: _tabController,
                  unselectedLabelColor: Colors.grey,
                  unselectedLabelStyle:
                  TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.w800, fontSize: 14),
                  labelColor: Colors.black,
                  tabs: <Widget>[
                    new Tab(text: 'Psycology'),
                    new Tab(text: 'Cardiology'),
                    new Tab(text: 'Gastrology'),
                    new Tab(text: 'Neurology'),
                  ],
                ),
              ),
              new Container(
                height: 225,
                child: new TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    new Container(
                        child: new SpecialistDoctors(special: 'psycology')),
                    new Container(
                        child: new SpecialistDoctors(special: 'cardiology')),
                    new Container(
                        child: new SpecialistDoctors(special: 'gastrology')),
                    new Container(
                        child: new SpecialistDoctors(special: 'neurology')),
                  ],
                ),
              )
            ],
          )
        ]));

    return new Scaffold(
        appBar: _homeAppBar,
        body: new Container(
            color: Colors.white,
            child: new DraggableScrollableSheet(
                initialChildSize: 0.98,
                minChildSize: 0.9,
                maxChildSize: 0.99,
                builder: (context, scrollController) =>
                    Container(
                        child: new SingleChildScrollView(
                          controller: scrollController,
                          child: new Column(children: [
                            new SizedBox(
                              height: 5,
                            ),
                            _specialisedIn,
                            new SizedBox(
                              height: 5,
                            ),
                            _doctors,
                            new SizedBox(
                              height: 5,
                            ),
                          ]),
                        )))));
  }
}
