

import 'package:flutter/material.dart';

class Money {
  final double iranRial;
  
  Money({@required this.iranRial,});

  double getDollar(){
    return this.iranRial * 0.000024;
  }

  double getRupee(){
    return this.iranRial * 0.0018;
  }


}