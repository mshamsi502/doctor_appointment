import 'package:doctor_appointment/main_home_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "HiDoc.",
      theme: ThemeData(
        primaryColor: new Color(0xff0148fa),
        accentColor: new Color(0xff5189f9),
      ),

      routes: {
        '/': (context) => new MainHomePage(),

      }, // for pushes without translate data between layout
      debugShowCheckedModeBanner: false,
    );
  }

}
